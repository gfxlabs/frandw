package frandw

import "lukechampine.com/frand"

var max = 0

func init() {
	max = len(WordMap)
}
func RandomWord() string {
	return WordMap[frand.Intn(max)-1]
}
