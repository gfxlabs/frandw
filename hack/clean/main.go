package main

import (
	"bufio"
	"log"
	"os"
	"strings"
	"text/template"
)

func main() {
	idx := 0
	wordset := map[string]uint64{}
	files := []string{}
	dirs, _ := os.ReadDir("./_data")
	for _, dir := range dirs {
		if !dir.IsDir() {
			files = append(files, "./_data/"+dir.Name())
		}
	}
	for _, filename := range files {
		log.Println("doing", filename)
		bd, err := os.ReadFile(filename)
		if err != nil {
			log.Panicln(err)
		}
		rd := strings.NewReader(string(bd))
		sc := bufio.NewScanner(rd)
		for sc.Scan() {
			line := string(sc.Bytes())
			words := strings.Split(line, " ")
			for _, vv := range words {
				if !strings.Contains(vv, ".") {
					v := strip(vv)
					if len(v) > 2 {
						if _, ok := wordset[v]; !ok {
							wordset[v] = uint64(idx)
							idx = idx + 1
						}
					}
				}
			}
		}
	}
	log.Println("generating go file")
	nf, err := os.Create("./wordlist.go")
	if err != nil {
		log.Panicln(err)
	}
	tmp := `package frandw

var WordMap = []string{ {{range $key, $val := .}}
	"{{$key}}", {{end}}
}
`
	//var WordMapBackwards = map[string]uint64{ {{range $key, $val := .}}
	//	"{{$key}}": {{$val}}, {{end}}
	//}

	wordtmp := template.Must(template.New("wordlist").Parse(tmp))
	wordtmp.Execute(nf, wordset)
}

func strip(s string) string {
	var result strings.Builder
	for i := 0; i < len(s); i++ {
		b := s[i]
		if ('a' <= b && b <= 'z') ||
			('A' <= b && b <= 'Z') ||
			b == ' ' {
			result.WriteByte(b)
		}
	}
	return strings.ToLower(result.String())
}
